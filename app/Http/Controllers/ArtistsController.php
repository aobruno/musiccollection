<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;

class ArtistsController extends Controller
{
    /**
     * Display the artists list view.
     *
     * @return \Illuminate\View\View
     */
    public function list()
    {
        $artists = $this->orderedArtists();

        return (count($artists) > 0) ? 
            view('artists', ['artists' => $artists]) : 
            view('artists')->withErrors(['error']);
    }

    /**
     * Remove one level of the array returned by the API and orders the result by artists name.
     *
     * @return array $artists
     */
    static function orderedArtists()
    {
        $artists = Http::withHeaders(['Basic' => 'ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=='])
            ->get('https://moat.ai/api/task/')->json();

        if (!empty($artists)) {
            foreach ($artists as $key => $artist) {
                $artists[$key] = $artist[0];
            }

            usort($artists, function ($a, $b) {   
                    return ($a['name'] < $b['name']) ? -1 : 1;
                });

            return $artists;
        } else {
            return [];
        }
    }
}
