<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ArtistsController;
use App\Models\Album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class AlbumsController extends Controller
{
    /**
     * Display the album list view.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $albums = Album::all();

        $artists = ArtistsController::orderedArtists();

        foreach ($albums as $album) {
            $album->artist_name = (count($artists) > 0) ? 
                $artists[array_search($album->artist, array_column($artists, 'id'))]['name'] :
                'Error!';
        }

        return view('albums.index', ['albums' => $albums]);
    }

    /**
     * Display the album creation view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $artists = ArtistsController::orderedArtists();

        return (count($artists) > 0) ? 
            view('albums.create', ['artists' => $artists]) : 
            view('albums.create')->withErrors(['error']);
    }

    /**
     * Handle an incoming album creation request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'year' => 'required|digits:4',
            'artist' => 'required',
        ]);

        $album = Album::create([
            'name' => $request->name,
            'year' => $request->year,
            'artist' => $request->artist,
        ]);

        return redirect()->action('App\Http\Controllers\AlbumsController@index');
    }

    /**
     * Display the album show view.
     *
     * @param  integer $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $album = Album::find($id);

        $artists = ArtistsController::orderedArtists();

        return (count($artists) > 0) ? 
            view('albums.show', ['album' => $album, 'artists' => $artists]) : 
            view('albums.show', ['album' => $album])->withErrors(['error']);
    }

    /**
     * Display the album update view.
     *
     * @param  integer $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $album = Album::find($id);

        $artists = ArtistsController::orderedArtists();

        return (count($artists) > 0) ? 
            view('albums.update', ['album' => $album, 'artists' => $artists]) : 
            view('albums.update', ['album' => $album])->withErrors(['error']);
    }

    /**
     * Handle an incoming album update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'year' => 'required|digits:4',
            'artist' => 'required',
        ]);

        $album = Album::find($id);

        $album->update([
            'name' => $request->name,
            'year' => $request->year,
            'artist' => $request->artist,
        ]);

        return redirect()->action('App\Http\Controllers\AlbumsController@index');
    }

    /**
     * Handle an incoming album delete request.
     *
     * @param  integer $id
     * @return \Illuminate\View\View
     */
    public function delete($id)
    {
        if (Auth::user()->role != 'admin') {    
            return response()->json(['error'], 403);
        }

        $album = Album::find($id);

        $album->delete();

        return response()->json(['success']);
    }
}
