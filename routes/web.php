<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AlbumsController;
use App\Http\Controllers\ArtistsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // Redirect to login page
    return view('auth.login');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

    // [Artists routes]
    // List artists
    Route::get('/artists', [ArtistsController::class, 'list'])
        ->name('artists');

    // [Albums routes]
    // List albums
    Route::get('/albums', [AlbumsController::class, 'index'])
        ->name('albums');

    // Redirect to create album view
    Route::get('/albums/create', [AlbumsController::class, 'create']);

    // Create album
    Route::post('/albums/create', [AlbumsController::class, 'store'])
        ->name('create_album');

    // Redirect to show album view
    Route::get('/albums/show/{id}', [AlbumsController::class, 'show'])
        ->name('show_album');

    // Redirect to update album view
    Route::get('/albums/update/{id}', [AlbumsController::class, 'edit']);

    // Update album
    Route::post('/albums/update/{id}', [AlbumsController::class, 'update'])
        ->name('update_album');

    // Delete album
    Route::get('/albums/delete/{id}', [AlbumsController::class, 'delete'])
        ->name('delete_album');

    // Routes created by Material Dashboard
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Auth::routes();
require __DIR__.'/auth.php';