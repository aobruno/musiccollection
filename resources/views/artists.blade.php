@extends('layouts.app', ['activePage' => 'artists', 'titlePage' => __('Artists List')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Artists</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            @if ($errors->count() > 0)
                            <p style="color: red">Sorry! No artists to show!</p>
                            @else
                            <select id="artist" class="form-control" name="artist" required>
                                @foreach($artists as $artist)
                                <table class="table">
                                    <tbody>
                                        @foreach ($artists as $artist)
                                        <tr>
                                            <td class="text-left"><a href="http://twitter.com/{{ str_replace('@', '', $artist['twitter']) }}" target="_blank">{{ $artist['name'] }}</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection