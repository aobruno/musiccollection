@extends('layouts.app', ['activePage' => 'albums', 'titlePage' => __('Albums List')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success d-none" role="alert">
                    Album deleted!
                    <button type="button" class="close" style="color: white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-danger d-none" role="alert">
                    You are not authorized to delete an album!
                    <button type="button" class="close" style="color: white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Albums</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Year</th>
                                        <th>Artist</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($albums as $album)
                                    <tr>
                                        <td>{{ $album->name }}</td>
                                        <td>{{ $album->year }}</td>
                                        <td>{{ $album->artist_name }}</td>
                                        <td class="td-actions">
                                            <button type="button" rel="tooltip" class="btn btn-info">
                                                <a href="{{ route('show_album', $album->id) }}" style="color: white"><i class="material-icons">person</i></a>
                                            </button>
                                            <button type="button" rel="tooltip" class="btn btn-success">
                                                <a href="{{ route('update_album', $album->id) }}" style="color: white"><i class="material-icons">edit</i></a>
                                            </button>
                                            <button type="button" rel="tooltip" class="btn btn-danger delete-button" data-toggle="modal" data-target="#deleteAlbumModal" data-album="{{ $album->id }}">
                                                <i class="material-icons">close</i>
                                                <!-- <a href="{{ route('delete_album', $album->id) }}" style="color: white"><i class="material-icons">close</i></a> -->
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="deleteAlbumModal" tabindex="-1" role="dialog"  aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Delete Album</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>This album will be deleted! Confirm?</p>
            </div>
            <div class="modal-footer">
                <input id="deleteAlbumId" type="hidden">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="confirmDelete" data-dismiss="modal">Yes</button>
            </div>
        </div>
    </div>
</div>
@endsection