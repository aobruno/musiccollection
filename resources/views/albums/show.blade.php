@extends('layouts.app', ['activePage' => 'album', 'titlePage' => __('Album')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Album</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" id="name" name="name" value="{{ $album->name }}" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" id="year" name="year" value="{{ $album->year }}" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="artist">Artist</label>
                                @if ($errors->count() > 0)
                                <p style="color: red">Sorry! No artists to show!</p>
                                @else
                                <select id="artist" class="form-control" name="artist" required>
                                    @foreach($artists as $artist)
                                    <option value="{{ $artist['id'] }}" {{ $artist['id'] == $album->artist ? 'selected' : '' }}>{{ $artist['name'] }}</option>
                                    @endforeach
                                </select>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
