@extends('layouts.app', ['activePage' => 'new_album', 'titlePage' => __('New Album')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">New Album</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form method="POST" action="{{ route('create_album') }}">
                                @csrf
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" id="name" placeholder="Name" name="name" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" id="year" placeholder="Year" name="year" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="artist">Artist</label>
                                    @if ($errors->count() > 0)
                                    <p style="color: red">Sorry! No artists to show!</p>
                                    @else
                                    <select id="artist" class="form-control" name="artist" required>
                                        @foreach($artists as $artist)
                                        <option value="{{ $artist['id'] }}">{{ $artist['name'] }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary" {{ $errors->count() > 0 ? 'disabled' : '' }}>Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
